rotina_menu();

$(function() {
    $(document).ready(function() {
        $("#assunto").select2({
            placeholder: "",
            allowClear: true,
        });
    })
})

$(function(){
    if(id_assunto !== undefined || id_assunto !== '' || id_assunto !== null){
        $("#assunto").select2().val(id_assunto).trigger("change");
    }
})

function select() {
    var valor = document.getElementById('assunto').value;
    if (!valor) {
        $('.teste').show();
        return null;
    }
    if (valor !== null || valor !== '' || valor !== undefined) {
        $('.teste').hide();
        return null;
    }
}

function limpa_formulario() {
    $('#contact-name').val('').blur();
    $('#contact-phone').val('').blur();
    $('#contact-email').val('').blur();
    $('#contact-message').val('').blur();
}

function envia_contato() {
    var nome = $('#contact-name').val();
    var telefone = $('#contact-phone').val();
    var email = $('#contact-email').val();
    var msg = $('#contact-message').val();
    var assunto = document.getElementById('assunto').value;

    if (!assunto) {
        let msg = 'Selecione o assunto!'
        alert_error(msg);
        return null;
    }

    var contato = {
        nome: nome,
        assunto: assunto,
        telefone: telefone,
        email: email,
        msg: msg
    };

    if ((nome == '' || nome == null) ||
        (email == '' || email == null) ||
        (msg == '' || msg == null)) {
        let msg = 'Os campos com *(asterisco) são de preenchimento obrigatório!'
        alert_error(msg);
        return null;
    }

    $.ajax({
        url: base_url + 'contato/contato',
        method: 'post',
        data: {
            nome: nome,
            assunto: assunto,
            telefone: telefone,
            email: email,
            msg: msg
        },
        dataType: 'json',
        success: function(data) {
            if (data.msg_success) {
                alert_success(data.msg_success, contato);
            } else {
                alert_error(data.msg_error, contato);
            }
        }
    })
}

function alert_success(msg, contato) {
    Swal.fire(
        msg,
        'Para agilizar o retorno, lhe encaminharemos para o whatsapp',
        'success'
    ).then(() => {
        contato_whatsapp(contato);
        window.location.href = base_url + 'contato';
    })
}

function alert_error(msg, contato) {
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: msg
    })
}