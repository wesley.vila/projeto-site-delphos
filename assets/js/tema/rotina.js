function rotina_menu() {
    $('#menu-footer-padrao').hide();
    $('#menu-padrao').hide();
    $('#endereco-top').hide();
    let html = '';
    let html_menu_footer = '';
    html += '<ul id="" class="rd-navbar-nav">' +
        '<li class="rd-nav-item"><a class="rd-nav-link" href="' + base_url + '">Início</a></li>' +
        '<li class="rd-nav-item"><a class="rd-nav-link" href="' + base_url + '#solucoes">Soluções</a></li>' +
        '<li class="rd-nav-item"><a class="rd-nav-link" href="' + base_url + '#depoimentos">Depoimentos</a></li>' +
        '<li class="rd-nav-item"><a class="rd-nav-link" href="' + base_url + 'downloads">Downloads</a></li>' +
        '<li class="rd-nav-item"><a class="rd-nav-link" href="' + base_url + 'contato">Contato</a></li>' +
        '</ul>' +
        '<div id="endereco-top2">' +
        '<div class="borda-top"></div>' +
        '<ul class="list-endereco">' +
        '<li><a href="<?php echo URL_WHATS ?>" target="_blank"><span class="icon text-middle fa-phone"></span> (94) 99198-1463 </a></li>' +
        '<li><a href="mailto:#"><span class="icon text-middle fa-envelope"></span> atendimento@delphos.com.br</a></li>' +
        '<li><a href="#" style="cursor:pointer;" onclick="modal_mapa()"><span class="icon text-middle fa-map-marker"></span> Av 7 de Setembro,Nº 10 - Sala 6, Centro - Tucuruí - PA</a></li>' +
        '</ul>' +
        '<ul class="social-links social-top">' +
        '<li><a class="icon icon-sm icon-circle icon-circle-md icon-bg-white fa-facebook" href="https://www.facebook.com/delphosautomacao/"></a></li>' +
        '<li><a class="icon icon-sm icon-circle icon-circle-md icon-bg-white fa-instagram" href="https://www.instagram.com/delphosautomacao/"></a></li>' +
        '</ul>' +
        '</div>';
    $('.rd-navbar-nav-wrap').append(html);

    html_menu_footer += '<ul id="" class="footer-classic-nav-list">' +
        '<li><a href="' + base_url + '">Início</a></li>' +
        '<li><a href="' + base_url + '#solucoes">Soluções</a></li>' +
        '<li><a href="' + base_url + '#depoimentos">Depoimentos</a></li>' +
        '<li><a href="' + base_url + 'downloads">Downloads</a></li>' +
        '<li><a href="' + base_url + 'contato">Contato</a></li>';
    '</ul>';
    $('#menu-footer').append(html_menu_footer);
}


function contato_whatsapp(contato) {
    var ct = numero_whats;

    var nome = contato.nome;
    var assunto = contato.assunto;
    var telefone = contato.telefone;
    var email = contato.email;
    var mensagem = contato.msg;
    var msg = 'Meu nome: ' + nome + '\n' +
        'Assunto: ' + assunto + '\n' +
        'Meu telefone: ' + telefone + '\n' +
        'Meu email: ' + email + '\n' +
        'Mensagem: ' + mensagem;

    // font: 
    let isMobile = (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
            return true;
        } else {
            return false;
        }
    })(navigator.userAgent || navigator.vendor || window.opera)

    let url = '';
    if (isMobile) {
        url = `https://api.whatsapp.com/send?phone=${encodeURIComponent(ct)}&text=${encodeURIComponent(msg)}`;
    } else {
        url = `https://web.whatsapp.com/send?phone=${encodeURIComponent(ct)}&text=${encodeURIComponent(msg)}`;
    }

    let a = document.createElement('a')
    a.target = '_blank'
    a.href = url // o link URL do api.whatsapp.com
    a.click()
}

function contatoProdutos(assunto){
    var ct = numero_whats;
    var msg = 'Assunto: ' + assunto + '\n';

    // font: 
    let isMobile = (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) {
            return true;
        } else {
            return false;
        }
    })(navigator.userAgent || navigator.vendor || window.opera)

    let url = '';
    if (isMobile) {
        url = `https://api.whatsapp.com/send?phone=${encodeURIComponent(ct)}&text=${encodeURIComponent(msg)}`;
    } else {
        url = `https://web.whatsapp.com/send?phone=${encodeURIComponent(ct)}&text=${encodeURIComponent(msg)}`;
    }

    let a = document.createElement('a')
    a.target = '_blank'
    a.href = url // o link URL do api.whatsapp.com
    a.click()
}