<?php

namespace app\controllers;

use app\core\Controller;

class HomeController extends Controller
{
    public function index()
    {
        //$dados['js_home'] = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>';
        $dados['js_home'] = '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>';
        $dados['js_home'] .= '<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>';
        $dados['js_home'] .= '<script src="'.URL_BASE.'assets/OwlCarousel2-2.3.4/docs/assets/vendors/jquery.min.js" type="text/javascript"></script>';
        $dados['js_home'] .= '<script src="'.URL_BASE.'assets/OwlCarousel2-2.3.4/dist/owl.carousel.js" type="text/javascript"></script>';
        $dados['js_home'] .= '<script class="cssdeck" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/js/bootstrap.min.js"></script>';
        $dados["view"] = "home/index";
        $this->load("template", $dados);
    }
}
