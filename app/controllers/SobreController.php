<?php

namespace app\controllers;

use app\core\Controller;

class SobreController extends Controller
{
    public function index()
    {
        $dados["view"] = "sobre/index";
        $this->load("template", $dados);
    }
}
