<?php

namespace app\controllers;

use app\core\Controller;
use app\classes\supports\Email;
use stdClass;

class ContatoController extends Controller
{
    public function index($val = null)
    {
        $dados["demo_repre"] = (@$val != null) ? $val->id : null;
        $dados['js_contato'] = $this->js();
        $dados['assuntos'] = $this->assunto();
        $dados["view"] = "contato/index";
        $this->load("template", $dados);
    }

    public function quero_teste($id = null)
    {
        $val = new stdClass;
        $val->id = $id;
        $this->index($val);
    }

    private function js()
    {
        $js = '<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc="crossorigin="anonymous"></script>';
        $js .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.full.js" defer></script>';
        $js .= '<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet"/>';
        $js .= '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>';
        $js .= '<script src="' . URL_BASE . 'assets/js/pg_contato/contato.js"></script>';
        return $js;
    }

    private function assunto()
    {
        return (object) array(
            (object) array(
                "id" => 1,
                "desc" => 'Preciso de uma solução'
            ),
            (object) array(
                "id" => 2,
                "desc" => "Quero uma demostração"
            ),
            (object) array(
                "id" => 3,
                "desc" => "Quero ser um representante"
            ),
            (object) array(
                "id" => 4,
                "desc" => "Tenho uma sugestão"
            )
        );
    }

    public function contato()
    {
        $email = new Email;
        $contato = new stdClass;
        $contato->nome      = $_POST["nome"];
        $contato->assunto   = $_POST["assunto"];
        $contato->telefone  = $_POST["telefone"];
        $contato->email     = $_POST["email"];
        $contato->msg       = $_POST["msg"];

        $assuntos = $this->assunto();
        foreach($assuntos as $assunto){
            if($assunto->id == $contato->assunto){
                $contato->assunto = $assunto->desc;
            }
        }

        $email->add(
            $contato->assunto,
            'Nome: ' . $contato->nome . '<br>' .
                'Telefone: ' . $contato->telefone . '<br>' .
                'Email: ' . $contato->email . '<br>' .
                'Mensagem: ' . $contato->msg,
            $contato->nome,
            EMAIL_ADM
        )->send();
        if (!$email->error) {
            $dados['msg_success'] = "Sua mensagem foi enviada com sucesso!";
        } else {
            $dados['msg_error'] = "Houve um erro durante a sua requisição";
            //echo $email->error()->getMessage();
        }
        echo json_encode($dados);
    }
}
