<?php

namespace app\controllers;

use app\core\Controller;

class DownloadsController extends Controller
{
    public function index()
    {
        $dados["view"] = "downloads/index";
        $this->load("template", $dados);
    }
}
