<?php

namespace app\controllers;

use app\core\Controller;

class SolucaoController extends Controller
{
    /*public function index()
    {
        $dados["view"] = "solucao/index";
        $this->load("template", $dados);
    }*/

    public function retaguarda(){
        $dados["view"] = "solucao/retaguarda/index";
        $this->load("template", $dados);
    }

    public function pdvFront(){
        $dados["view"] = "solucao/pdv-front/index";
        $this->load("template", $dados);
    }

    public function notas(){
        $dados["view"] = "solucao/notas/index";
        $this->load("template", $dados);
    }

    public function farma(){
        $dados["view"] = "solucao/farma/index";
        $this->load("template", $dados);
    }
}
