<section class="section main-section parallax-scene-js" style="background:url('<?php echo URL_BASE ?>assets/images/imagem-3.jpg') no-repeat center center; background-size:cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
                <h1 class="font-weight-bold wow fadeInLeft text-white">Contato</h1>
                <p class="intro-description wow fadeInRight text-white">Olá, tudo bem? Nós do Delphos Automação gostamos bastante de bater um papo!<br> Por isso, caso você tenha alguma dúvida, fique à vontade para entrar em contato com a gente através do formulário abaixo!</p>
                <!--<p class="intro-description wow fadeInRight text-white">Por favor, não deixe de entrar em contato conosco!!</p>-->
            </div>
        </div>
    </div>
</section>
<!--Mailform-->
<section class="section section-md">
    <div class="container">
        <!--RD Mailform-->
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-6 col-12 imagem-time wow slideInRight" data-wow-delay=".5s">
                <img style="max-width:500px;" src="<?php echo URL_BASE ?>assets/images/img-contato-2.png" alt="" />
            </div>
            <div class="col-xl-6 col-md-6 col-12">
                <form class="" data-form-output="" data-form-type="" method="" action="">
                    <div class="form-wrap">
                        <label class="form-label" for="contact-name"><strong>Nome / Razão</strong><span class="req-symbol">*</span></label>
                        <input class="form-input" id="contact-name" type="text" name="name" data-constraints="@Required">
                    </div>

                    <div class="form-wrap">
                        <label class="form-label teste" for="assunto"><strong>Assunto</strong><span class="req-symbol">*</span></label>
                        <select class="form-control" id="assunto" onchange="select()" name="assunto" data-constraints="@Required">
                            <option value=""></option>
                            <?php foreach ($assuntos as $assunto) {
                                echo "<option value='$assunto->id' $selecionado>$assunto->desc</option>";
                            }?>
                        </select>
                    </div>
                    <div class="form-wrap">
                        <label class="form-label" for="contact-phone"><strong>Telefone</strong><span class="req-symbol"></span></label>
                        <input class="form-input" id="contact-phone" type="text" name="phone" data-constraints="@PhoneNumber">
                    </div>
                    <div class="form-wrap">
                        <label class="form-label" for="contact-email"><strong>Email</strong><span class="req-symbol">*</span></label>
                        <input class="form-input" id="contact-email" type="email" name="email" data-constraints="@Required @Email">
                    </div>
                    <div class="form-wrap">
                        <label class="form-label label-textarea" for="contact-message"><strong>Mensagem</strong><span class="req-symbol">*</span></label>
                        <textarea class="form-input" id="contact-message" name="message" data-constraints="@Required"></textarea>
                    </div>
                    <!--Google captcha
                    <div class="form-wrap text-left form-validation-left">
                        <div class="recaptcha" id="captcha1" data-sitekey="6LfZlSETAAAAAC5VW4R4tQP8Am_to4bM3dddxkEt"></div>
                    </div>-->
                    <div class="form-button group-sm text-center text-lg-left">
                        <button onclick="limpa_formulario()" class="button button-danger" type="button" style="border-radius: 50px;">Limpar</button>
                        <button onclick="envia_contato()" class="button btn-primary-rounded-retaguarda" type="button" style="border-radius: 50px;">Enviar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<section class="section section-md">
    <div class="container">
        <!--<h3 class="wow fadeInLeft" data-wow-delay=".2s">Central de Ajuda</h3>
        <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">A Central de <a href="#" target="_blank" rel="noopener noreferrer">Ajuda</a> é nossa ferramenta de colaboração para auxiliar principalmente nossos clientes a iniciar o uso das nossas soluções e solucionar algumas situações que podem ocorrer no dia-a-dia. Estando sempre atualizada com as principais dúvidas e situações desenvolvidas a hora que precisar.</p>

        <div class="mt-5 mb-5">
            <hr>
        </div>-->
        <div class="row">
            <div class="col-md-6">
                <h3 class="wow fadeInLeft" data-wow-delay=".2s">Vídeos Tutorias</h3>
                <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">Também temos uma plataforma recheada de passo a passo e explicações valiosas para você aproveitar todas as funcionalidades do sistema. Veja Vídeos Tutoriais realmente eficientes.</p>

                <div class="mt-5 mb-5">
                    <hr>
                </div>

                <h3 class="wow fadeInLeft" data-wow-delay=".2s">Redes Sociais</h3>
                <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">Siga nossas redes sociais e fique por dentro das novidades!</p>
                <div class="wow fadeInUp">
                    <ul class="social-links social-contato">
                        <li><a class="" href="https://www.facebook.com/delphosautomacao/" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a></li>
                        <li><a class="" href="https://www.instagram.com/delphosautomacao/" target="_blank"><i class="fab fa-instagram fa-2x"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6">
                <div style="margin-bottom: -10px;">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1990.5967961578474!2d-49.67260109630964!3d-3.7680099382775083!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc177ad4820787e7d!2zRGVscGhvcyBBdXRvbWHDp8Ojbw!5e0!3m2!1spt-BR!2sbr!4v1596056544263!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'app/views/part-tema/footer.php' ?>
<style>
    .select2-selection__rendered {
        line-height: 50px !important;
    }


    .select2-container .select2-selection--single {
        height: 50px !important;
        border-radius: 8px !important;
        border-color: #d7d7d7 !important;
    }

    .select2-selection__arrow {
        height: 50px !important;
    }

    .social-contato li {
        font-size: 25px;
        padding-top: 15px;
        padding-right: 10px;
    }

    .social-contato li a {
        color: #08753F;
    }

    .social-contato li a:hover {
        color: #075a31;
    }
</style>
<script>
    var id_assunto = "<?php echo @$demo_repre?>";
</script>
<?= $js_contato ?>