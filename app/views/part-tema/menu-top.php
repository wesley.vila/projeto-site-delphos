<div class="rd-navbar-main-element">
    <div class="rd-navbar-nav-wrap">
        <ul id="menu-padrao" class="rd-navbar-nav">
            <li class="rd-nav-item"><a class="rd-nav-link" href="<?php echo URL_BASE ?>">Início</a>
            </li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="#" data-custom-scroll-to="solucoes">Soluções</a>
            </li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="#" data-custom-scroll-to="depoimentos">Depoimentos</a>
            </li>
            <li class="rd-nav-item"><a class="rd-nav-link" href="<?php echo URL_BASE ?>downloads">Downloads</a>
            </li>
            <!--<li class="rd-nav-item"><a class="rd-nav-link" href="typography.html">Typography</a>
            </li>-->
            <li class="rd-nav-item"><a class="rd-nav-link" href="<?php echo URL_BASE ?>contato">Contato</a>
            </li>
        </ul>
        <div id="endereco-top">
            <div class="borda-top"></div>
            <ul class="list-endereco">
                <li><a href="#" target="_blank"><span class="icon text-middle fa-phone"></span> (94) 99198-1463 </a></li>
                <li><a href="mailto:#"><span class="icon text-middle fa-envelope"></span> atendimento@delphos.com.br</a></li>
                <li><a href="#" style="cursor:pointer;" onclick="modal_mapa()"><span class="icon text-middle fa-map-marker"></span> Av 7 de Setembro,Nº 10 - Sala 6, Centro - Tucuruí - PA</a></li>
            </ul>
            <ul class="social-links social-top">
                <li><a class="icon icon-sm icon-circle icon-circle-md icon-bg-white fa-facebook" href="https://www.facebook.com/delphosautomacao/"></a></li>
                <li><a class="icon icon-sm icon-circle icon-circle-md icon-bg-white fa-instagram" href="https://www.instagram.com/delphosautomacao/"></a></li>
            </ul>
        </div>
    </div>
</div>

<style>
    @media screen and (min-width: 480px) {
        #endereco-top {
            display: none !important;
        }

        #endereco-top2 {
            display: none !important;
        }
    }

    @media only screen and (max-width: 480px) {
        .rd-nav-link {
            font-weight: bold;
        }
    }

    .borda-top {
        border-top: 2px solid #e9ecef;
        max-width: 130px;
        margin: 0 auto;
    }

    .list-endereco li a {
        color: #9b9b9b;
    }

    .list-endereco li {
        padding: 5px 15px 5px 18px;
    }

    .social-top {
        align-items: center;
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
    }
</style>