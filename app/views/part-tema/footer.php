        <!--Footer-->
        <footer class="section footer-classic section-sm">
            <div class="container">
                <div class="row row-30">
                    <!--<div class="col-lg-2 wow fadeInLeft">
                        <a class="brand" href="index.html"><img class="brand-logo-dark" src="<?php echo URL_BASE ?>assets/images/logo-default-200x34.png" alt="" width="100" height="17" /><img class="brand-logo-light" src="<?php echo URL_BASE ?>assets/images/logo-topo-delphos.png" alt="" width="100" height="17" /></a>
                    </div>-->
                    <div class="col-lg-4 wow fadeInUp">
                        <P class="footer-classic-title">Fale com a gente</P>
                        <div class="d-block offset-top-0">Av 7 de Setembro, Nº 10 altos sala 6, Centro <span class="d-lg-block">Tucuruí - PA</span></div>
                        <div><a class="d-inline-block accent-link" href="mailto:#" target="_blank">contato@delphosautomacao.com.br</a></div>
                        <div><a class="d-inline-block" href="https://api.whatsapp.com/send?phone=<?php echo NUMERO_WHATS?>&text=Ol%C3%A1%2C%20venho%20do%20site%20da%20Delphos%20Automa%C3%A7%C3%A3o" target="_blank">(94) 3787-1729</a></div>
                    </div>
                    <div class="col-lg-2 wow fadeInUp" data-wow-delay=".3s">
                        <?php include 'menu-footer.php' ?>
                    </div>
                    <div class="col-lg-3 wow fadeInUp">
                        <P class="footer-classic-title">Portais</P>
                        <ul class="footer-classic-nav-list">
                            <!--<li><span class="fa fa-link"></span><a href="http://helpdesk.delphosautomacao.com.br/"> HelpDesk</a></li>-->
                            <li><span class="fa fa-link"></span><a href="http://delphosautomacao.com.br/representantes/" target="_blank"> Portal do Representante</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 wow fadeInUp">
                        <ul class="social-links social">
                            <li><a class="fa fa-facebook" href="https://www.facebook.com/delphosautomacao/" target="_blank"></a></li>
                            <li><a class="fa fa-instagram" href="https://www.instagram.com/delphosautomacao/" target="_blank"></a></li>
                        </ul>
                        <div class="botao-footer">
                            <div align="center" class="wow fadeInRight" data-wow-delay=".2s"><a class="button button-circle btn-primary-rounded-retaguarda" style="" href="<?php echo URL_BASE. "contato/quero_teste/" . 2?>">Solicite uma demonstração</a></div>
                            <!--<div align="center" class="wow fadeInRight" data-wow-delay=".2s"><button class="button button-circle btn-primary-rounded-retaguarda mt-2" style="padding:5px 10px;" href="<?php echo URL_BASE ?>contato"><small>Solicitar contato</small></button></div>-->
                            <div align="center"><a class="d-inline-block" href="<?php echo URL_SUPORTE ?>" target="_blank">Solicitar Suporte</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container wow fadeInUp" data-wow-delay=".4s">
                <div style="border-top: 1px solid rgba(255, 255, 255, 0.1);"></div>
                <p class="rights" align="center">Copyright <span>&copy;&nbsp;</span><span class="copyright-year"></span> Delphos Automação - automação comercial ao seu alcance
                    <!--. Todos os direitos reservados. Design by <a class="btn-dv" onclick="infoDesenvolvedor()">Wesley Vila Seca</a>-->
                </p>
            </div>
            </div>
        </footer>

        <style>
            .botao-footer a{
                font-family: 'Montserrat', serif !important;
                font-size: 16px;
                text-transform: none;
                font-weight: 400 !important; 
                padding: 5px 10px !important;
                letter-spacing: 0px !important;
            }
            .footer-classic-title {
                color: rgba(25, 171, 99, 1) !important;
            }

            .social {
                align-items: center;
                display: flex;
                flex-direction: row;
                flex-wrap: wrap;
                justify-content: center;
            }
        </style>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
        <script type="">

            var ourLocation = document.URL;
  function infoDesenvolvedor(){
    Swal.fire({
      title: '<h5>Precisando de um site? entre em contato!</h5>',
      width: 500,
      type: 'info',
      html:  '<h6><img width="20px" src="https://img.icons8.com/color/16/000000/whatsapp.png"> - Meu <a href="https://api.whatsapp.com/send?phone=559198820-3132">WhatsApp</a> </h6><br>',
      showCloseButton: true,})
  }
</script>