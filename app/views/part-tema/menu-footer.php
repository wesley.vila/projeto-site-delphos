<P class="footer-classic-title">Menu</P>
<div id="menu-footer"></div>
<ul id="menu-footer-padrao" class="footer-classic-nav-list">
    <li><a href="<?php echo URL_BASE?>">Início</a></li>
    <li><a href="#" data-custom-scroll-to="solucoes">Soluções</a></li>
    <li><a href="#" data-custom-scroll-to="depoimentos">Depoimentos</a></li>
    <li><a href="<?php echo URL_BASE?>downloads">Downloads</a></li>
    <li><a href="<?php echo URL_BASE?>contato">Contato</a></li>
</ul>