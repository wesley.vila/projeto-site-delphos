<header class="section page-header">
    <!--RD Navbar-->
    <div class="rd-navbar-wrap">
        <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div id="icon-nav-endereco" class="rd-navbar-collapse-toggle rd-navbar-fixed-element-1" data-rd-navbar-toggle=".rd-navbar-collapse"><span></span></div>
            <div id="nav-endereco" class="rd-navbar-aside-outer rd-navbar-collapse bg-gray-dark">
                <div class="rd-navbar-aside">
                    <ul class="list-inline navbar-contact-list">
                        <li>
                            <div class="unit unit-spacing-xs align-items-center">
                                <div class="unit-left"><span class="icon text-middle fa-phone"></span></div>
                                <div class="unit-body"><a href="https://api.whatsapp.com/send?phone=<?php echo NUMERO_WHATS?>&text=Ol%C3%A1%2C%20venho%20do%20site%20da%20Delphos%20Automa%C3%A7%C3%A3o" target="_blank">(94) 99198-1463 </a></div>
                            </div>
                        </li>
                        <li>
                            <div class="unit unit-spacing-xs align-items-center">
                                <div class="unit-left"><span class="icon text-middle fa-envelope"></span></div>
                                <div class="unit-body"><a href="mailto:#" target="_blank">contato@delphosautomacao.com.br</a></div>
                            </div>
                        </li>
                        <li>
                            <div class="unit unit-spacing-xs align-items-center">
                                <div class="unit-left"><span class="icon text-middle fa-map-marker"></span></div>
                                <div class="unit-body"><a style="cursor: pointer;" onclick="modal_mapa()">Av 7 de Setembro, Nº 10 - Sala 6, Centro - Tucuruí - PA</a></div>
                            </div>
                        </li>
                    </ul>
                    <ul class="social-links">
                        <li><a class="icon icon-sm icon-circle icon-circle-md icon-bg-white fa-facebook" href="https://www.facebook.com/delphosautomacao/" target="_blank"></a></li>
                        <li><a class="icon icon-sm icon-circle icon-circle-md icon-bg-white fa-instagram" href="https://www.instagram.com/delphosautomacao/" target="_blank"></a></li>
                    </ul>
                </div>
            </div>
            <div class="rd-navbar-main-outer">
                <div class="rd-navbar-main">
                    <!--RD Navbar Panel-->
                    <div class="rd-navbar-panel">
                        <!--RD Navbar Toggle-->
                        <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                        <!--RD Navbar Brand-->
                        <div class="rd-navbar-brand">
                            <!--Brand--><a class="brand" href="index.html"><img class="brand-logo-dark" src="<?php echo URL_BASE ?>assets/images/logo-delphos-1.png" alt="" width="" height="" /><img class="brand-logo-light" src="<?php echo URL_BASE ?>assets/images/logo-inverse-200x34.png" alt="" width="100" height="17" /></a>
                        </div>
                    </div>
                    <?php include 'menu-top.php' ?>
                </div>
            </div>
        </nav>
    </div>
</header>

<!--modal mapa-->
<div class="modal fade" id="modal-map" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nossa Localização</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1990.5967961578474!2d-49.67260109630964!3d-3.7680099382775083!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc177ad4820787e7d!2zRGVscGhvcyBBdXRvbWHDp8Ojbw!5e0!3m2!1spt-BR!2sbr!4v1596056544263!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<style>
    @media only screen and (max-width: 480px) {
        #icon-nav-endereco {
            display: none !important;
        }
    }
</style>
<script type="text/javascript">
    function modal_mapa() {
        $("#modal-map").modal('show');
    }
</script>