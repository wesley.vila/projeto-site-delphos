<section class="section main-section parallax-scene-js" style="background:url('<?php echo URL_BASE ?>assets/images/imagem-3.jpg') no-repeat center center; background-size:cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
                <h1 class="font-weight-bold wow fadeInLeft text-white">Downloads</h1>
                <p class="intro-description wow fadeInRight text-white">Confira os downloads adicionais de suporte e colaboração de área de trabalho remota, que utilizamos para suporte a nossos clientes</p>
            </div>
        </div>
    </div>
</section>

<section class="section section-sm position-relative">
    <div class="container">
        <div class="row row-30">
            <div class="col-lg-7 col-12 sobre2">
                <div class="block-sm offset-top-45">
                    <div class="section-name wow fadeInRight mt-5" data-wow-delay=".2s"></div>
                    <div>
                        <div>
                            <div class="section-name wow fadeInRight mt-5 font-weight-bold" style="color: #08753F;">TeamViewer <img style="max-width: 20px;" src="<?php echo URL_BASE ?>assets/images/icone-team.jpg" alt="" /></div>
                            <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">O TeamViewer é um programa de acesso remoto, utilizado para receber suporte à distância de nossos técnicos! Após baixar, basta abrir e informar aos nosso técnicos o número que aparece e irão acessar para lhe ajudar!</p>
                            <div align="center" class="wow fadeInRight" data-wow-delay=".2s"><a class="button button-circle btn-primary-rounded-retaguarda" href="https://get.teamviewer.com/kc43naj">Download</a></div>
                        </div>

                        <div>
                            <div class="section-name wow fadeInRight mt-5 font-weight-bold" style="color: #08753F;">Anydesk <img style="max-width: 20px;" src="<?php echo URL_BASE ?>assets/images/icon-anydesk.png" alt="" /></div>
                            <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">Pode utilizar o Anydesk como opção também de suporte! Após baixar, basta abrir e informar aos nosso técnicos o número que aparece e irão acessar para lhe ajudar!</p>
                            <div align="center" class="wow fadeInRight" data-wow-delay=".2s"><a class="button button-circle btn-primary-rounded-retaguarda" href="https://download.anydesk.com/AnyDesk.exe">Download</a></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 mt-5">
                <div class="block-decorate-img wow fadeInLeft" data-wow-delay=".2s"><img style="max-height: 442px;" src="<?php echo URL_BASE ?>assets/images/neco3.png" alt="" />
                </div>
            </div>

        </div>
    </div>
    <div class="decor-text decor-text-1"></div>
</section>

<?php include 'app/views/part-tema/footer.php' ?>

<style>
    .texto-donwload {
        margin-top: 10px;

    }

    /**não funciona */
    @media (max-width: 566px) {
        .texto-donwload {
            margin-top: 5px;
        }
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    rotina_menu();
</script>