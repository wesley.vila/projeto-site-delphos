<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.css">
<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
<!--Main section-->
<section class="section main-section parallax-scene-js" style="background:url('<?php echo URL_BASE ?>assets/images/imagem-3.jpg') no-repeat center center; background-size:cover;">
    <div class="decorate-layer">
        <div class="layer-1">
            <div class="layer" data-depth=".20"><img src="<?php echo URL_BASE ?>assets/images/parallax-item-1-563x532.png" alt="" width="563" height="266" />
            </div>
        </div>
        <div class="layer-2">
            <div class="layer" data-depth=".30"><img src="<?php echo URL_BASE ?>assets/images/parallax-item-2-276x343.png" alt="" width="276" height="171" />
            </div>
        </div>
        <div class="layer-3">
            <div class="layer" data-depth=".40"><img src="<?php echo URL_BASE ?>assets/images/parallax-item-3-153x144.png" alt="" width="153" height="72" />
            </div>
        </div>
        <div class="layer-4">
            <div class="layer" data-depth=".20"><img src="<?php echo URL_BASE ?>assets/images/parallax-item-4-69x74.png" alt="" width="69" height="37" />
            </div>
        </div>
        <div class="layer-5">
            <div class="layer" data-depth=".40"><img src="<?php echo URL_BASE ?>assets/images/parallax-item-5-72x75.png" alt="" width="72" height="37" />
            </div>
        </div>
        <div class="layer-6">
            <div class="layer" data-depth=".30"><img src="<?php echo URL_BASE ?>assets/images/parallax-item-6-45x54.png" alt="" width="45" height="27" />
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-7 col-12">
                <div class="main-decorated-box text-center text-xl-left">
                    <h1 class="text-white text-xl-left wow slideInRight" data-wow-delay=".3s"><span class="align-top offset-top-30 d-inline-block font-weight-light prefix-text">As</span><span class="big font-weight-bold d-inline-flex offset-right-170">Melhores</span><span class="biggest d-block d-xl-flex font-weight-bold">Soluções</span></h1>
                    <div class="decorated-subtitle text-italic text-white wow slideInLeft">automação comercial ao seu alcance</div>
                </div>
                <div class="container" id="btn-top">
                    <div class="row">
                        <div class="col-md-4 mb-2" align="center" id="bnt-saiba"><a class="button button-190 button-circle btn-primary-rounded-fullgreen" href="#" data-custom-scroll-to="solucoes">Saiba mais</a></div>
                        <div class="col-md-5 mb-2" align="center"><a class="button button-190 button-circle btn-primary-rounded-fullgreen" href="<?php echo URL_BASE . "contato/quero_teste/" . 3 ?>">Quero ser um representante</a></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-12" id="img-top" align="center">
                <img class="imagem-ideia" src="<?php echo URL_BASE ?>assets/images/imagem-2-2.png" alt="">
            </div>
            <div class="col-12 text-center offset-top-75" data-wow-delay=".2s"><a class="button-way-point d-inline-block text-center d-inline-flex flex-column justify-content-center" href="#" data-custom-scroll-to="about"><span class="fa-chevron-down"></span></a></div>
        </div>
    </div>
</section>
<!--About-->
<section class="section section-sm position-relative" id="">
    <div class="container">
        <div class="row row-30">
            <div class="col-lg-5 mt-5">
                <div class="block-decorate-img wow fadeInLeft" data-wow-delay=".2s" id="img-sobre-a-empresa" align="center"><img src="<?php echo URL_BASE ?>assets/images/o-que-fazemos-8.png" alt="" width="570" height="351" />
                </div>
            </div>
            <div class="sobre1" id=""></div>
            <div class="col-lg-7 col-12 sobre2" id="">
                <div class="block-sm offset-top-45">
                    <div class="section-name wow fadeInRight mt-5" data-wow-delay=".2s"></div>
                    <h3 class="wow fadeInLeft devider-bottom" data-wow-delay=".3s">Sobre <span class="text-primary"> nossa empresa</span></h3>
                    <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">A Delphos Automação é uma empresa de desenvolvimento de sistemas focada em automação comercial que foi criada em 2012 e hoje tem como seu principal produto o BR SISTEMAS. Atuamos no segmento atacadista e varejista e trabalhamos com planejamento estratégico e principalmente focando nas necessidades de nossos clientes.</p>
                    <div>
                        <div class="section-name wow fadeInRight mt-5 font-weight-bold" style="color: #08753F;">Missão</div>
                        <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">Desenvolver soluções em sistemas de informação e serviços de consultoria que contribuam para melhorar os resultados da gestão comercial.</p>

                        <div class="section-name wow fadeInRight mt-5 font-weight-bold" style="color: #08753F;">Compromisso</div>
                        <p class="offset-xl-40 wow fadeInUp" data-wow-delay=".4s">O maior benefício que oferecemos aos nossos clientes é o valor que agregamos aos seus serviços e resultados.Posicionamo-nos como parceiros comprometidos, investindo permanentemente para melhor satisfazê-lo.</p>

                        <!--<p class="default-letter-spacing font-weight-bold text-gray-dark wow fadeInUp" data-wow-delay=".4s">Atuamos no segmento atacadista e varejista e trabalhamos com planejamento estratégico, tendo como foco principal as necessidades de nossos clientes.</p>
                                <a class="button-width-190 button-primary button-circle button-lg button offset-top-30" href="#">Leia mais</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="decor-text decor-text-1"></div>
</section>
<!--Features-->
<section class="section custom-section position-relative section-md">
    <div class="container">
        <div class="row">
            <div class="col-xl-7 col-lg-6 col-12">
                <div class="section-name wow fadeInRight"></div>
                <h3 class="devider-left wow fadeInLeft" data-wow-delay=".2s">Porque empresas usam<span class="text-primary"> Delphos Automação?</span></h3>
                <div class="subtitle">
                    <p>A mais de 8 anos no mercado, temos muito a oferecer para nossos clientes. Aqui estão algumas razões pelas quais as empresas em todo o mundo nos escolhem.</p>
                </div>
                <div class="row row-15">
                    <div class="col-xl-6 wow fadeInUp" data-wow-delay=".2s">
                        <div class="box-default">
                            <div class="box-default-title">IMPLANTAÇÃO RÁPIDA</div>
                            <p>Não fique meses implantando um sistema, solicite agora e use uma de nossas soluções para realizar sua próxima venda!</p><span style="color: #08753F ;" class="box-default-icon fa fa-clock-o fa-2x"></span>
                        </div>
                    </div>
                    <div class="col-xl-6 wow fadeInUp" data-wow-delay=".3s">
                        <div class="box-default">
                            <div class="box-default-title">SISTEMAS SEGUROS</div>
                            <p>As soluções da Delphos Automação são altamente seguras! Todos os seus dados diáriamente enviados para nosso servidor protegido. </p><span style="color: #08753F ;" class="box-default-icon fa fa-lock fa-2x"></span>
                        </div>
                    </div>
                    <div class="col-xl-6 wow fadeInUp" data-wow-delay=".4s">
                        <div class="box-default">
                            <div class="box-default-title">FUNCIONA OFFLINE</div>
                            <p>Nossas soluções funcionam em modo offline, com todas as operações básicas para o seu dia a dia. </p><span style="color: #08753F ;" class="box-default-icon fa fa-check-square-o fa-2x"></span>
                        </div>
                    </div>
                    <div class="col-xl-6 wow fadeInUp" data-wow-delay=".5s">
                        <div class="box-default">
                            <div class="box-default-title">RELATÓRIOS GERENCIAIS</div>
                            <p>Visualize em tempo real o estoque da sua loja, vendas efetuadas e tenha o controle total da sua empresa.</p><span style="color: #08753F ;" class="box-default-icon fa fa-line-chart fa-2x"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 col-12 imagem-time wow slideInRight" data-wow-delay=".5s">
                <img class="mt-5" src="<?php echo URL_BASE ?>assets/images/imagem-3-6.png" alt="" width="636" height="240" />
            </div>
        </div>
    </div>
    <div class="decor-text decor-text-2 wow fadeInUp" data-wow-delay=".3s"></div>
</section>

<!--Counters-->
<section class="section bg-image-2">
    <div class="container section-md">
        <div class="row row-30 text-center">
            <div class="col-xl-4 col-sm-4 col-12">
                <div class="counter-classic">
                    <div class="counter-classic-number"><span class="icon-lg novi-icon offset-right-10 mercury-icon-time"></span><span class="counter text-white" data-speed="2000">2012</span>
                    </div>
                    <div class="counter-classic-title text-white">Ano de fundação</div>
                </div>
            </div>
            <!--<div class="col-xl-3 col-sm-6 col-12">
                <div class="counter-classic">
                    <div class="counter-classic-number"><span class="icon-lg novi-icon offset-right-10 mercury-icon-gear"></span><span class="counter text-white" data-speed="2000">4893</span>
                    </div>
                    <div class="counter-classic-title text-white">Total de suporte atendido</div>
                </div>
            </div>-->
            <div class="col-xl-4 col-sm-4 col-12">
                <div class="counter-classic">
                    <div class="counter-classic-number"><span class="icon-lg novi-icon offset-right-10 mercury-icon-group"></span><span class="counter text-white" data-speed="2000">12</span><span class="symbol text-white"></span>
                    </div>
                    <div class="counter-classic-title text-white">Representantes</div>
                </div>
            </div>
            <div class="col-xl-4 col-sm-4 col-12">
                <div class="counter-classic">
                    <div class="counter-classic-number"><span class="icon-lg novi-icon offset-right-10 mercury-icon-partners"></span><span class="counter text-white" data-speed="2000">431</span>
                    </div>
                    <div class="counter-classic-title text-white">Total de clientes</div>
                </div>
            </div>
        </div>
    </div>
</section>

<!--cards sistemas por plano
<section class="section section-md bg-xs-overlay" style="background:url('<?php echo URL_BASE ?>assets/images/bg-plan.jpg')no-repeat;background-size:cover" id="solucoes">
    <div id="" class="container">
        <div class="row row-50 justify-content-center">
            <div class="col-12 text-center col-md-10 col-lg-8 subtitle">
                <div class="section-name wow fadeInRight" data-wow-delay=".2s"></div>
                <h3 class="wow fadeInLeft" data-wow-delay=".3s">Conheça nossas<span class="text-primary"> soluções</span></h3>
                <p>Soluções variadas com funcionalidades incríveis para atender as suas necessidades.</p>
            </div>
        </div>
        <div class="row row-30 justify-content-center">
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-gray-primary">
                    <div class="pricing-box-tittle">
                        <h3>LITE</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">110,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de vendas por garçom.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Atendimento pelo dispositivo android.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>CPF/CNPJ do consumidor no cupom.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Baixa de estoque automática online.</li>

                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-rounded-outline" href="#">Saiba mais</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-gray-primary">
                    <div class="pricing-box-tittle">
                        <h3>BÁSICO</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">199,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Venda de forma simples e rápida inserindo o codigo ou parte do nome do produto.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Envio e recebimento do retorno de envio do XML.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Envio de documento fiscal direto pro seu cliente.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Cadastros inteligente (Clientes/Produtos).</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Cálculos inteligente de ICMS.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Regras de CST/CFOP (Cálculo automático).</li>

                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-rounded-outline" href="#">Saiba mais</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-gray-primary">
                    <div class="pricing-box-tittle">
                        <h3>AVANÇADO</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">250,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Fechamento de caixa por operador.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Integração com balanças.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de operadores.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Suporta várias impressoras fiscais.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Sangria e suprimento de caixa fácil.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Diminui a fila no balcão.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Venda por código de barras ou teclado.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Transferência eletrônica de fundos (TEF).</li>
                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-rounded-outline" href="#">Saiba mais</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-gray-primary">
                    <div class="pricing-box-tittle">
                        <h3>PLUS</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">299,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Administração de compras</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Grades de produtos</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Administração financeira</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Situação cadastral clientes</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Suporta várias impressoras fiscais</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de seriais (Celulares, Máquinas)</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Cadastro de conta corrente</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de caixas</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Recebimento de carnê de crediário</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Orçamento personalizado (serviços e produtos)</li>
                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-rounded-outline" href="#">Saiba mais</a>
                </div>
            </div>
        </div>
    </div>
</section>-->

<!-- card por sistema-->
<section class="section section-md bg-xs-overlay" style="background:url('<?php echo URL_BASE ?>assets/images/bg-plan.jpg')no-repeat;background-size:cover" id="solucoes">
    <div id="" class="container">
        <div class="row row-50 justify-content-center">
            <div class="col-12 text-center col-md-10 col-lg-8 subtitle">
                <div class="section-name wow fadeInRight" data-wow-delay=".2s"></div>
                <h3 class="wow fadeInLeft" data-wow-delay=".3s">Conheça nossas<span class="text-primary"> soluções</span></h3>
                <p>Soluções variadas com funcionalidades incríveis para atender as suas necessidades.</p>
            </div>
        </div>
        <div class="row row-30 justify-content-center">
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-green">
                    <div class="pricing-box-tittle">
                        <h3 class="text-white">Retaguarda</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">0,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            LISTAGEM A DEFINIR
                            <br>
                            <br><br>
                            <!--<li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de vendas por garçom.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Atendimento pelo dispositivo android.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>CPF/CNPJ do consumidor no cupom.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Baixa de estoque automática online.</li>-->

                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-primary-rounded-voltar" href="<?php echo URL_BASE . 'solucao/retaguarda'?>">Saiba mais</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-blue">
                    <div class="pricing-box-tittle">
                        <h3 class="text-white">PDV Front loja</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">0,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            LISTAGEM A DEFINIR
                            <br>
                            <!--
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Venda de forma simples e rápida inserindo o codigo ou parte do nome do produto.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Envio e recebimento do retorno de envio do XML.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Envio de documento fiscal direto pro seu cliente.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Cadastros inteligente (Clientes/Produtos).</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Cálculos inteligente de ICMS.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Regras de CST/CFOP (Cálculo automático).</li>-->

                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-primary-rounded-pdv-front" href="<?php echo URL_BASE . 'solucao/pdvFront'?>">Saiba mais</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-blue-marinho">
                    <div class="pricing-box-tittle">
                        <h3 class="text-white">Notas</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">0,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            LISTAGEM A DEFINIR
                            <br>
                            <br><br>
                            <!--<li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Fechamento de caixa por operador.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Integração com balanças.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de operadores.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Suporta várias impressoras fiscais.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Sangria e suprimento de caixa fácil.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Diminui a fila no balcão.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Venda por código de barras ou teclado.</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Transferência eletrônica de fundos (TEF).</li>-->
                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-primary-rounded-notas" href="<?php echo URL_BASE . 'solucao/notas'?>">Saiba mais</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 col-12 wow fadeInUp" data-wow-delay=".4s">
                <div class="pricing-box bg-red">
                    <div class="pricing-box-tittle">
                        <h3 class="text-white">SimChef</h3>
                    </div>
                    <hr />
                    <div class="pricing-box-inner"><span class="pricing-box-symbol">R$</span><span class="pricing-box-price">0,00</span></div>
                    <div class="conteudo-plano">
                        <ul class="fa-ul">
                            LISTAGEM A DEFINIR
                            <br>
                            <br><br>
                            <!--<li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Administração de compras</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Grades de produtos</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Administração financeira</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Situação cadastral clientes</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Suporta várias impressoras fiscais</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de seriais (Celulares, Máquinas)</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Cadastro de conta corrente</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Controle de caixas</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Recebimento de carnê de crediário</li>
                            <li class="wow fadeInUp" data-wow-delay=".4s"><span class="fa-li"><i class="fa fa-chevron-right"></i></span>Orçamento personalizado (serviços e produtos)</li>-->
                        </ul>
                    </div>
                    <a class="button button-190 button-circle btn-primary-rounded-farma" href="<?php echo URL_BASE . 'solucao/farma'?>">Saiba mais</a>
                </div>
            </div>
        </div>
    </div>
</section>

<!--Owl Carousel-->
<section class="section section-md bg-gray-lighten" id="depoimentos">
    <div class="container">
        <div class="row justify-content-center flex">
            <div class="col-xl-10">
                <!--<div class="section-name wow fadeInRight text-center" data-wow-delay=".2s">testimonials</div>-->
                <h3 class="wow fadeInLeft text-center" data-wow-delay=".3s">O que os clientes dizem<span class="text-primary"> sobre nós</span></h3>
                <div class="owl-carousel review-carousel" data-items="1" data-sm-items="1" data-md-items="1" data-lg-items="1" data-xl-items="1" data-xxl-items="1" data-dots="true" data-nav="false" data-stage-padding="0" data-loop="true" data-margin="0" data-mouse-drag="" data-autoplay="">
                    <div class="item">
                        <div class="item-preview wow fadeInDown" data-wow-delay=".2s"><img src="<?php echo URL_BASE ?>assets/images/user-4.jpg" alt="" width="216" height="108" />
                        </div>
                        <div class="item-description wow fadeInUp" data-wow-delay=".3s">
                            <p>Agradeço a toda equipe da Delphos Automação, pelo trabalho, comprometimento, respeito e simplicidade com que atendem seus clientes, dando sempre todo suporte necessário.
                                Nunca me deixaram na mão quando eu precisei, tanto em relação ao sistema, quanto a situação que não era de responsabilidade da empresa e sempre me ajudam, somos parceiros desde 2012.
                            </p>
                            <div class="item-subsection"><span class="item-subsection-title devider-left">Elizeth</span><span>Drogaria Nova Esperança - BREU BRANCO </span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-preview wow fadeInDown" data-wow-delay=".2s"><img src="<?php echo URL_BASE ?>assets/images/lenny.jpg" alt="" width="216" height="108" />
                        </div>
                        <div class="item-description wow fadeInUp" data-wow-delay=".3s">
                            <p>Desde de Janeiro de 2014 a soluções da Delphos Automação foram muito mais que um controle de estoque, financeiro, vendas, relatórios etc... uma parceria que até os dias de hoje me ajuda e muito na gestão de minha loja, minhas dificuldades são solucionadas porque tenho o auxílio do acesso remoto, o que me ajuda a resolver as dificuldades que por ventura possam aparecer durante minha produção diária. </p>
                            <div class="item-subsection"><span class="item-subsection-title devider-left">Janilde cardoso</span><span>Lerray Perfumaria</span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-preview wow fadeInDown" data-wow-delay=".2s"><img src="<?php echo URL_BASE ?>assets/images/sbb.jpg" alt="" width="216" height="108" />
                        </div>
                        <div class="item-description wow fadeInUp" data-wow-delay=".3s">
                            <p>Empresa de ótima qualidade, sempre se qualificando pra melhor nos atender, só tenho a agradecer por tantos anos de parceria com a Delphos Automação e de todos os colaboradores, pois estão de parabéns.</p>
                            <div class="item-subsection"><span class="item-subsection-title devider-left">Angela Maria</span><span>Supermercado BemBem - ANAPU</span></div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="item-preview wow fadeInDown" data-wow-delay=".2s"><img src="<?php echo URL_BASE ?>assets/images/geracao.jpg" alt="" width="216" height="108" />
                        </div>
                        <div class="item-description wow fadeInUp" data-wow-delay=".3s">
                            <p>Já utilizo o BR Sistemas há 7 anos, e tem sido uma experiência muito gratificante, tenho um sistema confiável, com relatórios úteis e precisos, conto com um suporte técnico com atendimentos humanizado e eficiente, estou muito satisfeita.</p>
                            <div class="item-subsection"><span class="item-subsection-title devider-left">Lilian</span><span>Geração Eleita Confecções - NOVO REPARTIMENTO</span></div>
                        </div>
                    </div>
                    <!--<div class="item">
                        <div class="item-preview wow fadeInDown" data-wow-delay=".2s"><img src="<?php echo URL_BASE ?>assets/images/user-2.jpg" alt="" width="216" height="108" />
                        </div>
                        <div class="item-description wow fadeInUp" data-wow-delay=".3s">
                            <p>Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem </p>
                            <div class="item-subsection"><span class="item-subsection-title devider-left">Jennifer Coleman</span><span>Regular Client</span></div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
</section>

<div class="snackbars" id="form-output-global"></div>
<?php include 'app/views/part-tema/footer.php' ?>
<?= $js_home ?>
<script>
    var largura_display = screen.width;
    if (largura_display < 580) {
        $('.sobre2').prop('id', 'about');
    } else {
        $('.sobre1').prop('id', 'about');
        $('.sobre2').addClass('mt-5');
    }
</script>

<!--coded by Drel-->

<style>
    .ie-panel {
        display: none;
        background: #212121;
        padding: 10px 0;
        box-shadow: 3px 3px 5px 0 rgba(0, 0, 0, .3);
        clear: both;
        text-align: center;
        position: relative;
        z-index: 1;
    }

    html.ie-10 .ie-panel,
    html.lt-ie-10 .ie-panel {
        display: block;
    }

    .link-desc:hover {
        color: #40d173;
    }

    .imagem-time {
        max-width: 100%;
        height: 100%;
        margin: 50px auto 0 auto;
    }

    .conteudo-plano {
        padding-top: 20px;
        text-align: left;

    }

    #btn-saiba {
        padding-left: 0px !important;
    }

    #img-top img {
        padding: 0px 50px;
        max-width: 450px;
    }

    #img-sobre-a-empresa img {
        padding-top: 50px;
        width: 100%;
    }

    @media screen and (max-width: 700px) {
        #btn-saiba {
            padding-left: 15px !important;
        }

        #btn-top a {
            letter-spacing: 0.12em;
            font-size: 13px;
        }

        #img-top img {
            padding: 0px;
            max-width: 100%;
        }

        #img-sobre-a-empresa img {
            padding-top: 0px;
        }
    }
</style>