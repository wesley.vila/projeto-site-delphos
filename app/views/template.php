<!DOCTYPE html>
<html class="wide wow-animation" lang="en">
<?php include 'part-tema/head.php' ?>
<script>
    var base_url = "<?php echo URL_BASE ?>";
    var numero_whats = "<?php echo NUMERO_WHATS?>";
</script>
<script src="<?php echo URL_BASE ?>assets/js/tema/rotina.js"></script>
<body>
    <div class="ie-panel">
        <!--<a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo URL_BASE ?>assets/images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a>-->
    </div>
    <div class="preloader">
        <div class="preloader-body">
            <div class="cssload-container">
                <div class="cssload-speeding-wheel"></div>
            </div>
            <p>Carregando...</p>
        </div>
    </div>
    <div class="page">
        <?php include 'part-tema/header.php' ?>
        <?php $this->load($view, $viewData); ?>
    </div>
    <a href="https://api.whatsapp.com/send?phone=<?php echo NUMERO_WHATS?>&text=Ol%C3%A1%2C%20venho%20do%20site%20da%20Delphos%20Automa%C3%A7%C3%A3o" target="_blank"><img src="<?php echo URL_BASE ?>assets/images/whats.png" style="height:60px; position:fixed; bottom: 30px; left: 30px; z-index:100;" data-selector="img"></a>
</body>
<!--scripts do tema-->
<script src="<?php echo URL_BASE ?>assets/js/tema/core.min.js"></script>
<script src="<?php echo URL_BASE ?>assets/js/tema/script.js"></script>
</html>