<section class="section main-section parallax-scene-js" style="background:url('<?php echo URL_BASE ?>assets/images/imagem-3.jpg') no-repeat center center; background-size:cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
                <h1 class="font-weight-bold wow fadeInLeft text-white">PDV-FRONT LOJA</h1>
                <p class="intro-description wow fadeInRight text-white">PONTO DE VENDAS</p>
            </div>
        </div>
    </div>
</section>

<section class="section mt-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="offset-top-45 offset-lg-right-45">
                    <a class="button button-190 button-circle btn-primary-rounded-pdv-front mb-5" href="<?php echo URL_BASE ?>#solucoes"><span class="fa fa-chevron-left"></span> Voltar</a>
                    <div class="section-name wow fadeInRight" data-wow-delay=".2s"></div>
                    <h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s"><span class="text-primary"></span></h3>
                    <p class="font-weight-bold text-gray-dark wow fadeInUp" data-wow-delay=".4s">
                        <!--<div class="ifen"></div>
                        <h6 class="ml-5">GERENCIADOR</h6>-->
                    </p>
                    <p class="wow fadeInUp" data-wow-delay=".4s">
                        PDV-Front Loja foi desenvolvido para efetuar vendas para consumidor final, utilizando impressoras fiscais e o sistema de pagamento TEF. É ideal para quem deseja rapidez e eficiência. É um sistema completo e flexível para a gestão operacional do pequeno e médio comercio varejista.
                    </p>
                    <p class="wow fadeInUp" data-wow-delay=".4s">
                        PDV (Ponto de Venda ) – Realiza a venda ao consumidor, permitindo a escolha de diversas formas de pagamento e imprime cupom fiscal para os seguintes Modelos de ECF: Bematech / Daruma / Epson / Sweda / Elgin / Argox / Toleto / Tanca </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- About page about section-->
<section class="section section-md">
    <div class="container">
        <div class="row row-40 justify-content-center">
            <div class="col-lg-5 col-sm-10 col-12">
                <div class="block-decorate-img wow fadeInLeft" data-wow-delay=".2s" align="center"><img class="mt-5" style="max-width:350px;" src="<?php echo URL_BASE ?>assets/images/PDV-FRONT-LOJA.png" alt="" />
                </div>
                <h6 align="center" class="text-blue mb-5">imagem meramente ilustrativa</h6>
                <h3 class="wow fadeInLeft text-capitalize mb-3" data-wow-delay=".3s" align="center">R$200,00<span class="text-blue" style="font-size: 15px;">/mensal</span></h3>
                <h6 align="center">Gerenciador completo para toda a empresa</h6>
            </div>
            <div class="col-lg-7 col-12">
                <div class="offset-top-45 offset-lg-right-45">
                    <div class="section-name wow fadeInRight diferenciais-pdv" data-wow-delay=".2s">
                        <h5 class="text-blue diferenciais-title">DIFERENCIÁIS</h5>
                    </div>
                    <h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s"><span class="text-primary"></span></h3>
                    <div class="offset-top-20">
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Vendas com NFC-e homologado – Novo</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Fechamento de caixa por operador e muito mais…</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Baixa de estoque automática online.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Recurso com módulo pré venda.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Lançamento de venda direta modo check-out.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Consulta de preços.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Sangria e suprimento de caixa fácil.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Diminui a ﬁla no balcão.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Venda por código de barras ou teclado.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Indentiﬁcação do cliente no cupom.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Atende a legislação vigente (TEF)</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Suporta várias impressoras ﬁscais.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Transferência eletrônica de fundos (TEF).</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Integração com balanças. Controle de operadores.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #3C87A7 ;" class="fa fa-chevron-right"></span> Fechamento de caixa por operador e muito mais..</p>

                        <button class="button button-190 button-circle  btn-primary-rounded-pdv-front" onclick="contatoProdutos('Quero saber sobre o sistema PDV-Front')"><span class="fa fa-phone"></span> Entre em contato</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section bg-xs-overlay" style="padding: 10px;">
    <div class="container">
        <div class="row row-50 justify-content-center">
            <div class="col-12 text-center col-md-10 col-lg-8">
                <div class="section-name wow fadeInRight" data-wow-delay=".2s"></div>
                <!--<h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s">Conheça nossas<span class="text-primary"> soluções</span></h3>
                        <p>Soluções variadas com funcionalidades incríveis para atender as suas necessidades.</p>
                    </div>-->
            </div>
            <div class="row row-30 justify-content-center">

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue diferenciais-title">DIFERENCIAIS</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            A solução que você precisa.<br>
                            Offline: Funciona<br>
                            Independente do servidos, com o recurso oline e offline sen perda de recursos e vantagens.
                            </p>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue diferenciais-title">PERFORMANCE</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            Sistema completo para controlar sua empresa e de fácil manuseio, 
                            permitindo que vários terminais acessem um único banco de dados sem conflitos.</p>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue diferenciais-title">ATUALIZAÇÃO</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            Uma vez com a licença em dia, você têm acesso à todas as modificações
                             e à versão mais atualizada do sistema, sem cobranças extras por isso.</p>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue diferenciais-title">BACKUP AUTOMÁTICO</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            Com o backup automatizado, fique tranquilo caso você venha a perder seu computador, 
                            basta instalar o sistema em outra máquina.</p>
                    </div>
                </div>

            </div>
        </div>
</section>

<?php include 'app/views/part-tema/footer.php' ?>

<style>
    .ifen {
        border: 2px solid #08753f;
        width: 40px;
        position: absolute;
        margin-top: 12px;
    }

    .diferenciais {
        background: rgba(76, 175, 80, 0.3);
        border-radius: 15px;
    }

    .diferenciais-title {
        font-weight: bold;
        text-align: center;
    }

    .diferenciais-pdv {
        background: #cce5ff;
        border-radius: 15px;
    }
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    rotina_menu();
</script>