<section class="section main-section parallax-scene-js" style="background:url('<?php echo URL_BASE ?>assets/images/imagem-3.jpg') no-repeat center center; background-size:cover;">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8 text-center">
                <h1 class="font-weight-bold wow fadeInLeft text-white">NOTAS</h1>
                <p class="intro-description wow fadeInRight text-white">EMISSOR</p>
            </div>
        </div>
    </div>
</section>

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="offset-top-45 offset-lg-right-45">
                    <a class="button button-190 button-circle btn-primary-rounded-notas mb-5" href="<?php echo URL_BASE?>#solucoes"><span class="fa fa-chevron-left"></span> Voltar</a>
                    <div class="section-name wow fadeInRight" data-wow-delay=".2s"></div>
                    <h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s"><span class="text-primary"></span></h3>
                    <p class="font-weight-bold text-gray-dark wow fadeInUp" data-wow-delay=".4s">
                        <!--<div class="ifen"></div>
                        <h6 class="ml-5">GERENCIADOR</h6>-->
                    </p>
                    <p class="wow fadeInUp" data-wow-delay=".4s">
                    Notas é um módulo voltado a parte fiscal da empresa, desenvolvido para as empresas que já fazem uso de uma outra solução de gestão ou que irá apenas atender as legislações vigentes em relação à emissão de Nota Fiscal Eletrônica e querem usufruir de um leve e prático emissor de NF-e e NFC-e.                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- About page about section-->
<section class="section section-md">
    <div class="container">
        <div class="row row-40 justify-content-center">
            <div class="col-lg-5 col-sm-10 col-12">
                <div class="block-decorate-img wow fadeInLeft" data-wow-delay=".2s" align="center"><img class="mt-5" style="max-width:350px;" src="<?php echo URL_BASE ?>assets/images/NOTAS.png" alt="" />
                </div>
                <h6 align="center" class="text-blue-marinho mb-5">imagem meramente ilustrativa</h6>
                <h3 class="wow fadeInLeft text-capitalize mb-3" data-wow-delay=".3s" align="center">R$200,00<span class="text-blue-marinho" style="font-size: 15px;">/mensal</span></h3>
                <h6 align="center">Gerenciador completo para toda a empresa</h6>
            </div>
            <div class="col-lg-7 col-12">
                <div class="offset-top-45 offset-lg-right-45">
                    <div class="section-name wow fadeInRight diferenciais-notas" data-wow-delay=".2s">
                        <h5 class="text-white diferenciais-title">DIFERENCIÁIS</h5>
                    </div>
                    <h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s"><span class="text-primary"></span></h3>
                    <div class="offset-top-20">
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Importação de XML’s.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Cálculos inteligente de ICMS.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Envio e recebimento do retorno de envio do XML.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Cadastros inteligente (Clientes/Produtos).</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Envio de documento ﬁscal direto pro seu cliente.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Pré-Visualização do DANFE antes de autorizar.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Possibilidade para exportação dos xml’s.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> (Importe os xml’s de qualquer sistema sem complicações).</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Backup automatizado dos seus dados (Não se preocupe com cópias).</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Venda por código de barras ou teclado.</p>
                        <p class="wow fadeInUp" data-wow-delay=".4s"><span style="color: #4BC1CB ;" class="fa fa-chevron-right"></span> Envio automático de arquivos ﬁscais mensais.</p>

                        <button class="button button-190 button-circle btn-primary-rounded-notas" onclick="contatoProdutos('Quero saber sobre o sistema Notas')"><span class="fa fa-phone"></span> Entre em contato</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section bg-xs-overlay">
    <div class="container">
        <div class="row row-50 justify-content-center">
            <div class="col-12 text-center col-md-10 col-lg-8">
                <div class="section-name wow fadeInRight" data-wow-delay=".2s"></div>
                <!--<h3 class="wow fadeInLeft text-capitalize" data-wow-delay=".3s">Conheça nossas<span class="text-primary"> soluções</span></h3>
                        <p>Soluções variadas com funcionalidades incríveis para atender as suas necessidades.</p>
                    </div>-->
            </div>
            <div class="row row-30 justify-content-center">

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue-marinho diferenciais-title">CONFIABILIDADE</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            Um sistema testado, consolidado e elogiado pelos usuários. 
                            Tenha certeza de estar utilizando um produto que não vai lhe decepcionar.</p>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue-marinho diferenciais-title">PERFORMANCE</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            Sistema completo para controlar sua empresa e de fácil manuseio, 
                            permitindo que vários terminais acessem um único banco de dados sem conflitos.</p>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue-marinho diferenciais-title">ATUALIZAÇÃO</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            Uma vez com a licença em dia, você têm acesso à todas as modificações e
                             à versão mais atualizada do sistema, sem cobranças extras por isso.</p>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 col-12 wow fadeInDown" data-wow-delay=".3s">
                    <div class="box-text-solution bg-gray-dark-solucoes">
                        <div class="text-blue-marinho diferenciais-title">BACKUP AUTOMÁTICO</div>
                        <hr />
                        <p class="wow fadeInUp" data-wow-delay=".4s" align="left">
                            Com o backup automatizado, fique tranquilo caso você venha 
                            a perder seu computador, basta instalar o sistema em outra máquina.</p>
                    </div>
                </div>

            </div>
        </div>
</section>

<?php include 'app/views/part-tema/footer.php' ?>

<style>
    .ifen {
        border: 2px solid #08753f;
        width: 40px;
        position: absolute;
        margin-top: 12px;
    }

    .diferenciais {
        background: rgba(76, 175, 80, 0.3);
        border-radius: 15px;
    }

    .diferenciais-notas {
        background: #4BC1CB;
        border-radius: 15px;
    }

    .diferenciais-title {
        font-weight: bold;
        text-align: center;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    rotina_menu();
</script>